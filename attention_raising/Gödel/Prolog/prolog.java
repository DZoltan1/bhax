import java.util.ArrayList;


class Prolog {
	
	public static ArrayList<String> ferfi = new ArrayList <String>();
	public static ArrayList<String> no = new ArrayList <String>();
	public static String[][] gyereke = new String[8][2];

	public static void feltolt(){

		ferfi.add("Ágoston");
		ferfi.add("Béla");
		ferfi.add("Csaba");
		ferfi.add("Dávid");

		no.add("Eszter");
		no.add("Flóra");
		no.add("Gizella");
		no.add("Hedvig");

		gyereke[0][0] = ferfi.get(0); //apa Ágoston
		gyereke[0][1] = ferfi.get(2); //fia Csaba

		gyereke[1][0] = no.get(0); //anya Eszter
		gyereke[1][1] = ferfi.get(2); //fia Csaba

		gyereke[2][0] = ferfi.get(1); //apa Béla
		gyereke[2][1] = no.get(2); //lánya Gizella

		gyereke[3][0] = no.get(1); //anya Flóra
		gyereke[3][1] = no.get(2); //lánya Gizella

		gyereke[4][0] = ferfi.get(2); //apa Csaba
		gyereke[4][1] = ferfi.get(3); //fia Dávid

		gyereke[5][0] = no.get(2); //anya Gizella
		gyereke[5][1] = ferfi.get(3); // fia Dávid

		gyereke[6][0] = ferfi.get(2); //apa Csaba
		gyereke[6][1] = no.get(3); //lánya Hedvig

		gyereke[7][0] = no.get(2); //anya Gizella
		gyereke[7][1] = no.get(3); //lánya Hedvig


	}

	public static boolean ferfi(String nev){
		if(ferfi.contains(nev)){
			return true;
		}
		else return false;
	} 

	public static boolean no(String nev){
		if(no.contains(nev)){
			return true;
		}
		else return false;
	} 

	public static boolean gyerek(String szulo, String gyerek){
		for (int i = 0; i < 7; i++){
				
			if(gyereke[i][0] == szulo){
				if(gyereke[i][1] == gyerek) return true;
				else return false;
			}
		}
	return false;
	}
	public static boolean unokaja(String nagyszulo, String gyerek){
		for (int i = 0; i < 7; i++){
			if(gyereke[i][0] == nagyszulo){
				gyerek(gyereke[i][1], gyerek);
			}
		}

	return false;
	}

	public static void main(String[] args){

		feltolt();

		System.out.println(ferfi("Csaba"));
		System.out.println(ferfi("Gizella"));
		System.out.println(gyerek("Eszter","Csaba"));
		System.out.println(gyerek("Eszter", "Béla"));
		System.out.println(unokaja("Béla","Dávid"));
		System.out.println(unokaja("Béla", "Gizella"));


	}
	

}