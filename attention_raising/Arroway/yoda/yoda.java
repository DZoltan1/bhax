class yoda{

	static int myNumber = 42;
	static String myString = null;
	static int x = 0;


	public static void main(String args[]){

		try{	
			if(42 == myNumber) {
				System.out.println("yoda condition is OK(number == variable)");} //Yoda Conditon teljesül

			/*if(myNumber ==42) {
				throw new NullPointerException("conditional statment is illegal!");  //nem teljesül a Yoda Condition
			}

			String myString = null;
			if (myString.equals("foobar")) {   // Itt sem teljesül */

			
			if ("foobar".equals(myString)) { System.out.println("why?!");} //Itt teljesül, de ennek ellenére nem dob hibát */			

			if ((-1 <= x) && (x <= 1))	{System.out.println("yoda condition is ok, real number line");}		

			if ((x >= -1) && (x <= 1)){throw new NullPointerException("Not a real number line!");}

		}

		catch (NullPointerException e){
			System.out.println("Yoda condition problem:" + e);

		}

		catch (Exception e){

			System.out.println("Is there an other problem.." + e);
		}
	}

	/*public static  void check_variable(){
			if(42 == myNumber) {
				throw new Exception("yoda condition was OK");}
	}*/
}