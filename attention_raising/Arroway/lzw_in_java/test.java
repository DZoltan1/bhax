

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.math.RoundingMode;
import java.text.DecimalFormat;

class Test{

	private static DecimalFormat df = new DecimalFormat("0.00");

	public String test_input(){
		String content = "";

		try {
		content = new String ( Files.readAllBytes( Paths.get("./in.txt") ) ); }

		catch(Exception e){
			System.out.println(e);
		}
		if (content == "01111001001001000111"){
			return "smth...";
		}
		else System.out.println(content); return "Hibás a bemeneti adat";
	}

	public String test_output(double mean, double var){
		double rnd_mean = Math.round(mean*100); rnd_mean/=100;
		double rnd_var = Math.round(var*100); rnd_var/=100;
		if(rnd_mean == 5.58 && rnd_var == 1.00 )
			return "a teszt sikeres";
		else return "nem stimmelnek az adatok.";
	}
}