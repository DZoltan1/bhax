import java.util.Random;
import java.lang.Math; 

class Tarol{

	public Tarol() {}

	public double t_szam;
	public double r_szam;

	public boolean tarolt;

	Random rand = new Random();

	public double szamol(){

		if(tarolt) {

			tarolt = false; return t_szam;
		}

		else{


			double rand1, rand2, v1, v2, w_rand;

			do{
                rand1 = rand.nextDouble() / 1;
                rand2 = rand.nextDouble() / 1;
                v1 = 2 * rand1 - 1;
                v2 = 2 * rand2 - 1;
                w_rand = v1 * v1 + v2 * v2;

              }while(w_rand > 1);

            double r_szam = Math.sqrt((-2 * Math.log(w_rand)) / w_rand);

            t_szam = r_szam * v2;
            tarolt = true;

            return r_szam * v1;
		}
	}


}
class Polargen{


	public static void main(String args[]){

		Tarol tarol = new Tarol();

		for(int i = 0; i < 10; i++){

            System.out.println(tarol.szamol());


        }

	}
}
