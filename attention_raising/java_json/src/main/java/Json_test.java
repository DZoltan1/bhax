import com.fasterxml.jackson.databind.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Json_test {
    public static void main (String[] args){
        ObjectMapper obj = new ObjectMapper();
        List<Object> list_of_dealers = new ArrayList<>();

        carDealer dealer1 = new carDealer(1, "Thomas");
        carDealer dealer2 = new carDealer(2 , "Frank");

        Car car = new Car("yellow", "renault");
        Car car1 = new Car("black", "ford");
        Car car2 = new Car("blue", "ferrari");
        Car car3 = new Car("white", "mercedes");

        dealer1.add_new_car(car);
        dealer1.add_new_car(car1);

        dealer2.add_new_car(car2);
        dealer2.add_new_car(car3);

        list_of_dealers.add(dealer1);
        list_of_dealers.add(dealer2);

        try {
            obj.writeValue(new File("car.json"), list_of_dealers);
            System.out.println("Sikeres a json fájl létrehozása");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

class Car {
    public String color;
    public String type;

    public Car(String color, String type){
        this.color = color;
        this.type = type;
    }
}


class carDealer{

    public int id;
    public String name;

    public List<Object> List_of_car = new ArrayList<>();

    public void add_new_car(Car car){
        List_of_car.add(car);
    }

    public carDealer(int id , String name){
        this.id = id;
        this.name = name;
    }

}