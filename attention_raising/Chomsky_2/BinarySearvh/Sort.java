import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

class Sort {

    public static void main(String[] args) {
        List_of_items list = new List_of_items();
        list.print_list();
        list.add_item(8);
        System.out.println("\n-------------------------------------\n");
        list.print_list();
    }
}

class List_of_items{

    List<Integer> array = new ArrayList<>();
    int count_search = 0;
    int max_search = 0;

    public List_of_items(){
        array.add(3);
        array.add(20);
        array.add(15);
        array.add(5);
        array.add(10);
        sort();
    }

    public void sort(){
        for(int i = 0; i<array.size(); i++) {
            for (int j = 0; j < array.size(); j++) {
                if (array.get(i) < array.get(j)) {
                    Collections.swap(array, i, j);
                }
            }
        }

    }

    public void add_item(int new_item){
    	count_search = 0;
        max_search = counter_max(array.size());
        search_in_list(new_item, 0, array.size());
        sort();
    }

    public void search_in_list(int new_item, int i, int size){
        count_search ++;
        if(count_search == max_search+1){
            array.add(new_item);
        }

        else if (new_item == array.get((i+size) /2)){
            System.out.println("A szám benne van a listában: " + new_item + " Indexe: " + (i+size) / 2);

        }

        else if (new_item > array.get( (i+size) /2 )){
            search_in_list(new_item, (i+size)/2 , size);
        }

        else if(new_item < array.get (( i + size) / 2)){
            search_in_list(new_item, i, size/2);
        }

    }

    public void print_list(){
        array.forEach(System.out::println);
    } 


    public int counter_max(int length){
        int max_search = 0;

        while(length != 1){
            length /= 2;
            max_search++;
        }
        return max_search + 1;
    }
}
