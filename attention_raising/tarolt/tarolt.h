#ifndef TAROLT_H
#define TAROLT_H

#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iostream>

class Tarolt{
    double r_szam;
    double tarolt_szam;
    bool vanTarolt;

public:

    Tarolt();
    ~Tarolt() {}
    double szamolas();
};

#endif