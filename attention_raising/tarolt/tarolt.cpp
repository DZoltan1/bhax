#include "tarolt.h"

using namespace std;

Tarolt::Tarolt(){

    vanTarolt = false;
    srand(time(NULL));
    r_szam = szamolas();
}

double Tarolt::szamolas(){

    if(vanTarolt) {vanTarolt = false ; return tarolt_szam;}

    else {
        double rand1,rand2,v1,v2,w_rand;

        do{
            rand1 = rand() / (RAND_MAX + 1.0);
            rand2 = rand() / (RAND_MAX + 1.0);
            v1 = 2 * rand1 - 1;
            v2 = 2 * rand2 - 1;
            w_rand = v1 * v1 + v2 * v2;

        }while(w_rand > 1);

            double r_szam = sqrt((-2 * log (w_rand)) / w_rand);

            tarolt_szam = r_szam * v2;
            vanTarolt = true;

            return r_szam * v1;

    }
}