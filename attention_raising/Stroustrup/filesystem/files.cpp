#include <iostream>
#include <string>
#include <experimental/filesystem>
namespace filesys = std::experimental::filesystem::v1;

int main(){
    filesys::path pathToShow("/home/zltnd/Asztal/prog/src_unzip");

     for(auto& path_rec: filesys::recursive_directory_iterator(pathToShow)){
     	if (!filesys::is_directory(path_rec)){
        std::cout << path_rec.path() << '\n';}
    }
}