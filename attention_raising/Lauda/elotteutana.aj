public aspect Elotteutana{


	public pointcut fgvHívás() : call(static * public void AspectTest().testfgv());

	before() : fgvHívás() {
		System.out.println("Futás előtt");
	}

	after() : fgvHívás() {
		System.out.println("Futás után");
	}
}