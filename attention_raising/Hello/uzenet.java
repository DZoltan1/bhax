
import java.io.*;

class Main {
     static File file = new File("secret.txt");
    static BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {
        String tiszta_szoveg ="";
        while(true) {
            tiszta_szoveg = get_string_from_user();
            System.out.println("Beírt szó: " + tiszta_szoveg);
            String titkositott = caesarCode(tiszta_szoveg, 'F');
            write_to_file(titkositott + '\n');
        }

    }

    public static String caesarCode(String input, char offset) {
        char[] out = input.toCharArray();
        for (int i = 0; i < out.length; i++) {
            if(out[i] != '\n')
                out[i] += offset - 'A';
            if (out[i] > 'Z')
                out[i] -= 'Z' - 'A' + 1;
        }
        return new String(out);
    }

    public static void write_to_file(String secret)  {
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(file, true));
            writer.write(secret);
            writer.close();
        }
        catch(Exception e){
            System.out.println("Valami nem jó");
        }
    }

    public static String get_string_from_user(){
        try{
        String input_txt;
        input_txt = obj.readLine();
        return input_txt;
        }
        catch (Exception e){
            System.out.println("Hiba van a gépezetben");
            return "";
        }

    }
}
