#include <stdio.h>
#include <stdlib.h>

int
main ()
{
    int nr = 5; // a métrix mérete
    double **tm; // létrehozunk egy mátrixra mutató mutaót.
    
    printf("%p\n", &tm); //kiirítjuk a memóriacímét a mátrixunknak

    
    if ((tm = (double **) malloc (nr * sizeof (double *))) == NULL)
    {
        return -1;
    }
 
    // malloc(), digitális memóriakezelés c-ben, amihez az stdlib.h -t meg kellett hívnunk. A memóriában már fellelhető a mátrixunk, viszont ki lettek nullázva. Illetve értéket se tudunk ég adni hozzájuk. 
    
    printf("%p\n", &tm[0]);    
    printf("%p\n", tm[5]);

    
    for (int i = 0; i < nr; ++i)
    {
        if ((tm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)
        {
            return -1;
        }

    }

    //Hasonlóan az előző mallocos részhez a 



    
    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i+1) / 2  + j;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    tm[3][0] = 42.0;
    (*(tm+3))[1] = 43.0;	// mi van, ha itt hiányzik a külső ()
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;


    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

 
    for (int i = 0; i < nr; ++i)
        free (tm[i]);

    free (tm);


    return 0;
}
